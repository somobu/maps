package localhost.iillyyaa2033.maps.fragment;

import android.view.*;

import android.app.Fragment;
import android.os.Bundle;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import localhost.iillyyaa2033.maps.interfaces.MapFragmentInterface;
import localhost.iillyyaa2033.maps.logic.items.LayerMeta;
import localhost.iillyyaa2033.maps.logic.storages.DatabaseStorage;
import localhost.iillyyaa2033.maps.logic.storages.SettingsStorage;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.map.android.rendertheme.AssetsRenderTheme;
import org.mapsforge.map.datastore.MultiMapDataStore;
import org.mapsforge.map.datastore.Way;
import org.mapsforge.map.rendertheme.XmlRenderTheme;
import org.osmdroid.mapsforge.MapsForgeTileProvider;
import org.osmdroid.mapsforge.MapsForgeTileSource;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.util.SimpleRegisterReceiver;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import java.io.IOException;
import org.osmdroid.views.overlay.Overlay;
import android.graphics.Canvas;
import localhost.iillyyaa2033.maps.logic.maplayers.LocationOverlay;
import android.view.View.OnClickListener;

public class OsmdroidMapFragment extends Fragment implements MapFragmentInterface {

	MapView mapView;
	XmlRenderTheme rtheme;
	MapsForgeTileProvider provider;
	
	LocationOverlay loc;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		MapsForgeTileSource.createInstance(this.getActivity().getApplication());
		try {
			rtheme = new AssetsRenderTheme(getActivity().getApplicationContext(), "renderthemes/","rendertheme-v4.xml");
		} catch (IOException e) {}
		
		
		mapView = new MapView(getActivity());
		loc = new LocationOverlay();
		
		mapView.getController().setCenter(new GeoPoint(SettingsStorage.savedViewLatLong.latitude,SettingsStorage.savedViewLatLong.longitude));
        mapView.getController().setZoom(5);
        mapView.getController().zoomTo(5);
		mapView.setBuiltInZoomControls(SettingsStorage.ANDROID_MAP_ZOOMCONTROLS);
		mapView.getOverlayManager().add(loc);
		mapView.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					// TODO: Implement this method
				}
			});
		resyncLayers();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return mapView;
	}

	@Override
	public void resyncLayers() {
		switch(SettingsStorage.ANDROID_MAP_TYPE){
			case SettingsStorage.MapTypes.LOCAL:
				provider = new MapsForgeTileProvider(new SimpleRegisterReceiver(getActivity()), MapsForgeTileSource.createFromFiles(getLocalMaps(),rtheme, "rendertheme-v4"));
				provider.clearTileCache();
				
				mapView.setTileProvider(provider);
				mapView.setUseDataConnection(false);
				break;
			case SettingsStorage.MapTypes.MAPNIK:
				mapView.setUseDataConnection(true);
				mapView.setTileProvider(null);
				final ITileSource source = TileSourceFactory.MAPNIK;
				mapView.setTileSource(source);
				break;
			case SettingsStorage.MapTypes.USGS:
				mapView.setUseDataConnection(true);
				mapView.setTileProvider(null);
				final ITileSource tileSource = TileSourceFactory.USGS_TOPO;
				mapView.setTileSource(tileSource);
				break;
		}
	}

	@Override
	public void zoomIn() {
		mapView.getController().zoomIn();
	}

	@Override
	public void zoomOut() {
		mapView.getController().zoomOut();
	}

	@Override
	public void scrollTo(LatLong pos) {
		mapView.getController().animateTo(new GeoPoint(pos.latitude,pos.longitude));
	}

	@Override
	public void setLocationEnabled(boolean isEnabled) {
		// TODO: Implement this method
	}
	
	@Override
	public void setNewLocation(GeoPoint point, boolean isOnline) {
		if(loc != null){
			loc.setLocation(point,isOnline);
		}
	}
	
	@Override
	public List<Way> getVisibleWays() {
		// TODO: Implement this method
		return new ArrayList<Way>();
	}
	
	File[] getLocalMaps(){
		ArrayList<File> mapfiles = new ArrayList<File>();
		for(LayerMeta meta : DatabaseStorage.layers){
			if(meta.isDeleteable){
				File file = new File(meta.argument);
				if(file.exists()) mapfiles.add(file);
			}
		}
		File[] maps = new File[mapfiles.size()];
		maps = mapfiles.toArray(maps);
		return maps;
	}
}
