package localhost.iillyyaa2033.maps.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.interfaces.MapActivityInterface;
import localhost.iillyyaa2033.maps.logic.storages.SettingsStorage;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class MapsManagerFragment extends Fragment{
	
	MapActivityInterface activity;
	final String[] names = {"local","OpenStreetsMap","USGStopo"};
	SelectedAdapter adapter;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
            this.activity = (MapActivityInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement some interfaces");
        }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ListView view = new ListView(getActivity());
		adapter = new SelectedAdapter();
		view.setAdapter(adapter);
		view.setOnItemClickListener(new OnItemClickListener(){

				@Override
				public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4) {
					SettingsStorage.ANDROID_MAP_TYPE = p3;
					adapter.notifyDataSetChanged();
					activity.resyncLayers();
				}
			
		});
		return view;
	}
	
	class SelectedAdapter extends ArrayAdapter<String>{
		
		public SelectedAdapter(){
			super(getActivity(),0);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public String getItem(int position) {
			return names[position];
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View layout = getActivity().getLayoutInflater().inflate(R.layout.item_mapsmanager,parent,false);
			
			RadioButton ind = (RadioButton) layout.findViewById(R.id.item_mapsmanager_RadioButton);
			if(position==SettingsStorage.ANDROID_MAP_TYPE) ind.setChecked(true);
			else ind.setChecked(false);
			ind.setClickable(false);
			ind.setFocusable(false);
			ind.setFocusableInTouchMode(false);
			
			TextView view = (TextView) layout.findViewById(R.id.item_mapsmanager_TextView);
			view.setText(getItem(position));

			View divider = layout.findViewById(R.id.item_mapsmanager_divider);
			View button = layout.findViewById(R.id.item_mapsmanager_ImageButton);
			
			if(position != 0){
				divider.setVisibility(View.GONE);
				button.setVisibility(View.GONE);
			}
			
			button.setFocusable(false);
			button.setFocusableInTouchMode(false);
			button.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View p1) {
						activity.startLayerman();
					}
				});
			
			return layout;
		}

		@Override
		public int getCount() {
			return names.length;
		}
	}
}
