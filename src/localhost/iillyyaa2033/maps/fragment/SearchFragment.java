package localhost.iillyyaa2033.maps.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import java.util.ArrayList;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.interfaces.MapActivityInterface;
import localhost.iillyyaa2033.maps.interfaces.OnSearchListener;
import localhost.iillyyaa2033.maps.logic.items.SearchItem;

public class SearchFragment extends Fragment {

	private OnSearchListener searcher;
	private MapActivityInterface activity;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
            this.activity = (MapActivityInterface) activity;
			searcher = (OnSearchListener) activity;
			
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement all interfaces");
        }	
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LinearLayout view = (LinearLayout) inflater.inflate(R.layout.fragment_search,container,false);
		
		final EditText field = (EditText) view.findViewById(R.id.fragment_search_field);
		
		ImageButton btn = (ImageButton) view.findViewById(R.id.fragment_search_button);
		btn.setImageResource(android.R.drawable.ic_menu_search);
		btn.setOnClickListener(new View.OnClickListener(){

				@Override
				public void onClick(View p1) {
					searcher.search(field.getText().toString(), false);
				}
			});
		
		ListView list = (ListView) view.findViewById(R.id.fragment_search_list);
		final ArrayAdapter<SearchItem> adapter = new ArrayAdapter<SearchItem>(getActivity(),android.R.layout.simple_list_item_1);
		list.setAdapter(adapter);
		
		list.setOnItemClickListener(new OnItemClickListener(){

				@Override
				public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4) {
					SearchItem selected = adapter.getItem(p3);
					activity.scroll(selected.points[0][0],true);
				}
			});
		
		searcher.setOnSearchListener(new OnSearchListener.OnSearchResult(){

				@Override
				public void onResult(ArrayList<SearchItem> result) {
					adapter.clear();
					adapter.addAll(result);
					adapter.notifyDataSetChanged();
				}
			});
		
		searcher.search("",false);
			
		return view;
	}
}
