package localhost.iillyyaa2033.maps.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import com.mobeta.android.dslv.DragSortController;
import com.mobeta.android.dslv.DragSortListView;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.activity.EditNoteActivity;
import localhost.iillyyaa2033.maps.interfaces.MapActivityInterface;
import localhost.iillyyaa2033.maps.logic.items.Note;
import localhost.iillyyaa2033.maps.logic.storages.DatabaseStorage;

public class NotesListFragment extends Fragment{
	
	MapActivityInterface activity;
	ArrayAdapter adapter;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
            this.activity = (MapActivityInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement all interfaces");
        }
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.fragment_notes,container,false);
			
		DragSortListView list = (DragSortListView) ll.findViewById(R.id.fragment_notes_list);
		DragSortController controller = new DragSortController(list);
		controller.setDragHandleId(R.id.drag_handle);
        controller.setRemoveEnabled(true);
        controller.setSortEnabled(false);
        controller.setDragInitMode(DragSortController.ON_DOWN);
        controller.setRemoveMode(DragSortController.FLING_REMOVE);
		controller.setBackgroundColor(Color.argb(50, 0, 0, 0));
		list.setFloatViewManager(controller);
        list.setOnTouchListener(controller);
		list.setDragEnabled(true);
		list.setOnItemClickListener(new OnItemClickListener(){

				@Override
				public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4) {
					activity.scroll(DatabaseStorage.notes.get(p3).position,true);
				}
			});
		list.setOnItemLongClickListener(new OnItemLongClickListener(){

				@Override
				public boolean onItemLongClick(AdapterView<?> p1, View p2, int p3, long p4) {
					Intent i = new Intent(getActivity(),EditNoteActivity.class);
					i.putExtra("noteid",p3);
					startActivityForResult(i,0);
					return true;
				}
			});	
		list.setRemoveListener(new DragSortListView.RemoveListener(){

				@Override
				public void remove(int which) {
					DatabaseStorage.notes_remove(which);
					adapter.notifyDataSetChanged();
				}
			});

		adapter = new ArrayAdapter<Note>(getActivity(),android.R.layout.simple_list_item_1,DatabaseStorage.notes);
		list.setAdapter(adapter);
//		adapter = new LayersManagerAdapter(getActivity());
//		list.setAdapter(adapter);
			
		return ll;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == Activity.RESULT_OK){
			adapter.notifyDataSetChanged();
		}
	}
}
