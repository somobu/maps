package localhost.iillyyaa2033.maps.fragment;

import android.app.*;
import android.view.*;
import android.widget.*;

import android.content.Context;
import android.content.DialogInterface;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import java.util.List;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.interfaces.MapActivityInterface;
import localhost.iillyyaa2033.maps.logic.storages.SettingsStorage;

public class LocationFragment extends Fragment {

	String selectedLocationProvider = "passive";
	
	MapActivityInterface activity;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
            this.activity = (MapActivityInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement all interfaces");
        }
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		final List<String> providers = locationManager.getProviders(true);
		
		LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.fragment_location,container,false);
		ListView list = (ListView) ll.findViewById(R.id.fragment_location_ListView);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,providers);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener(){

				@Override
				public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4) {
					activity.listenFrom(providers.get(p3));
		//			if(!SettingsStorage.isTrackRecorderStarted) showEnableTrackRecorderDialog(providers.get(p3));
		//			else 
						activity.closePanel();
				}
			});
		
		Button button = (Button) ll.findViewById(R.id.fragment_location_Button);
		button.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					activity.scroll(SettingsStorage.savedLocationLatLong,true);
				}
			});
		return ll;
	}
	
	void showEnableTrackRecorderDialog(final String provider){
		new AlertDialog.Builder(getActivity())
			.setMessage("Do you want to record new track?")
			.setNegativeButton("No", new DialogInterface.OnClickListener(){

				@Override
				public void onClick(DialogInterface p1, int p2) {
					activity.closePanel();
				}
			})
			.setNeutralButton("Yes", new DialogInterface.OnClickListener(){

				@Override
				public void onClick(DialogInterface p1, int p2) {
					activity.startRecordTrack(provider);
					activity.closePanel();
				}
			})
			.setPositiveButton("Yes, show prefs", new DialogInterface.OnClickListener(){

				@Override
				public void onClick(DialogInterface p1, int p2) {
					activity.startRecordTrack(provider);
					activity.closePanel();
				}
			})
			.show();
	}
}
