package localhost.iillyyaa2033.maps.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.logic.items.LayerMeta;
import localhost.iillyyaa2033.maps.logic.storages.DatabaseStorage;

public class LayersManagerAdapter extends ArrayAdapter<LayerMeta>{
	
	public LayersManagerAdapter(Context c){
		super(c,R.layout.item_layersmanager);
	}

	@Override
	public int getCount() {
		return DatabaseStorage.layers.size();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.item_layersmanager, null);
		}
		
		LayerMeta current = DatabaseStorage.layers.get(position);
		
		TextView title = (TextView) convertView.findViewById(R.id.item_layersmanager_text_name);
		title.setText(current.name);
		
		TextView subtitle = (TextView) convertView.findViewById(R.id.item_layersmanager_text_subname);
		subtitle.setText(current.argument);
		
		return convertView;
	}

	@Override
	public int getPosition(LayerMeta item) {
		return DatabaseStorage.layers.indexOf(item);
	}

	@Override
	public LayerMeta getItem(int position) {
		return DatabaseStorage.layers.get(position);
	}
}
