package localhost.iillyyaa2033.maps.sw2;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.sonyericsson.extras.liveware.aef.registration.Registration;
import com.sonyericsson.extras.liveware.extension.util.ExtensionUtils;
import com.sonyericsson.extras.liveware.extension.util.registration.RegistrationInformation;
import java.util.UUID;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.activity.PreferenceSW2Activity;

public class CustomRegistrationInformation extends RegistrationInformation{

	Context context;
	String extensionKey;
	String EXTENSION_KEY_PREF = "localhost.iillyyaa2033.maps_sw2preferences";

	public CustomRegistrationInformation(Context c){
		context = c;
	}

	@Override
	public ContentValues getExtensionRegistrationConfiguration() {
		String iconHostapp = ExtensionUtils.getUriString(context, R.drawable.ic_launcher);
        String iconExtension = ExtensionUtils.getUriString(context, R.drawable.ic_launcher);
        String iconExtension48 = ExtensionUtils.getUriString(context, R.drawable.ic_launcher);
        String iconExtensionBw = ExtensionUtils.getUriString(context, R.drawable.ic_launcher);

        ContentValues values = new ContentValues();

        values.put(Registration.ExtensionColumns.CONFIGURATION_ACTIVITY, PreferenceSW2Activity.class.getName());
        values.put(Registration.ExtensionColumns.CONFIGURATION_TEXT, context.getString(R.string.sw2_config_text));
        values.put(Registration.ExtensionColumns.NAME, context.getString(R.string.sw2_extension_name));
        values.put(Registration.ExtensionColumns.EXTENSION_KEY, getExtensionKey());
        values.put(Registration.ExtensionColumns.HOST_APP_ICON_URI, iconHostapp);
        values.put(Registration.ExtensionColumns.EXTENSION_ICON_URI, iconExtension);
        values.put(Registration.ExtensionColumns.EXTENSION_48PX_ICON_URI, iconExtension48);
        values.put(Registration.ExtensionColumns.EXTENSION_ICON_URI_BLACK_WHITE, iconExtensionBw);
        values.put(Registration.ExtensionColumns.NOTIFICATION_API_VERSION, getRequiredNotificationApiVersion());
        values.put(Registration.ExtensionColumns.PACKAGE_NAME, context.getPackageName());

		return values;
	}

	@Override
    public synchronized String getExtensionKey() {
        if (TextUtils.isEmpty(extensionKey)) {
            SharedPreferences pref = context.getSharedPreferences(EXTENSION_KEY_PREF, Context.MODE_PRIVATE);
            extensionKey = pref.getString(EXTENSION_KEY_PREF, null);
            if (TextUtils.isEmpty(extensionKey)) {
                extensionKey = UUID.randomUUID().toString();
                pref.edit().putString(EXTENSION_KEY_PREF, extensionKey).commit();
            }
        }
        return extensionKey;
    }

	@Override
    public boolean isDisplaySizeSupported(int width, int height) {
		//       return (width == HelloLayoutsControl.getSupportedControlWidth(mContext) && height == HelloLayoutsControl.getSupportedControlHeight(mContext));
 		return true;
    }

	@Override
	public int getRequiredNotificationApiVersion() {
		return API_NOT_REQUIRED;
	}

	@Override
	public int getRequiredWidgetApiVersion() {
		return API_NOT_REQUIRED;
	}

	@Override
	public int getRequiredControlApiVersion() {
		return 2;
	}

	@Override
	public int getRequiredSensorApiVersion() {
		return API_NOT_REQUIRED;
	}
	
	@Override
    public boolean supportsLowPowerMode() {
        return true;
    }
	
	@Override
	public boolean controlInterceptsBackButton() {
        return true;
    }
}
