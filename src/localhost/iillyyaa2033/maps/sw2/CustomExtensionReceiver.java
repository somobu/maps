package localhost.iillyyaa2033.maps.sw2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class CustomExtensionReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		intent.setClass(context, CustomExtensionService.class);
        context.startService(intent);
	}

}
