package localhost.iillyyaa2033.maps.logic.items;

import org.mapsforge.core.model.LatLong;

public class SearchItem{
	
	public String name;
	public LatLong[][] points;
	
	public SearchItem(String name, LatLong[][] points){
		this.name = name;
		this.points = points;
	}

	@Override
	public String toString() {
		return name;
	}
}
