package localhost.iillyyaa2033.maps.logic.items;

import org.mapsforge.core.model.LatLong;

public class Note{
	
	public String tag;
	public LatLong position;
	public int color;
	
	public Note(String name, LatLong position, int color){
		this.tag = name;
		this.position = position;
		this.color = color;
	}

	@Override
	public String toString() {
		return tag;
	}
	
	
}
