package localhost.iillyyaa2033.maps.logic.items;

import java.util.ArrayList;
import org.mapsforge.core.model.LatLong;
import android.graphics.Color;

public class Track{
	
	public int id; // For database
	public ArrayList<TrackItem> items = new ArrayList<TrackItem>();
	// TODO: optimisation: replace ArrayList with long[] and double[] arrays
	
	public String name = "Unnamed track [time will be here]";
	public int color = Color.argb(100,255,0,0);
	public int lenght = 0;	// metres
	public long time = 0;	// mills
	public boolean mustBeDisplayed = true;
	
	public Track(){
		
	}
	
	public Track(int id, String name, int color, int lenght, long time, int md){
		this.id = id;
		this.name = name;
		this.color = color;
		this.lenght = lenght;
		this.time = time;
		mustBeDisplayed = md>0;
	}
	
	public static class TrackItem{
		public long time;
		public LatLong position;
		
		public TrackItem(long _time, LatLong _position){
			time = _time;
			position = _position;
		}
	}
}
