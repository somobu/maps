package localhost.iillyyaa2033.maps.logic.maplayers;

import android.graphics.*;

import localhost.iillyyaa2033.maps.logic.storages.SettingsStorage;
import org.osmdroid.util.BoundingBox;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;

public class LocationOverlay extends Overlay {

	private Paint paintFill;
	private Paint paintStroke;
	private volatile GeoPoint location;
	private volatile boolean isOnline;
	
	public LocationOverlay(){
		paintFill = new Paint();
		paintFill.setColor(Color.parseColor("#25B60A00"));
		
		paintStroke = new Paint();
		paintStroke.setStyle(Paint.Style.STROKE);
		paintStroke.setStrokeWidth(3f);
		paintStroke.setColor(Color.parseColor("#BBB60A00"));
		
		location = new GeoPoint(0,0);
		isOnline = true;
	}
	
	@Override
	protected void draw(Canvas canvas, MapView osmv, boolean shadow) {
		Point p = osmv.getProjection().toPixels(location, null);
		float lenght = osmv.getProjection().metersToPixels(30);
		
		canvas.drawCircle(p.x, p.y, lenght, paintFill);
		canvas.drawCircle(p.x, p.y, lenght, paintStroke);
	}

	public void setLocation(GeoPoint p, boolean isOnline){
		location = p;
		
		if(this.isOnline != isOnline){
			this.isOnline = isOnline;
			
			if(isOnline){
				paintFill.setColor(Color.parseColor("#25B60A00"));
				paintStroke.setColor(Color.parseColor("#BBB60A00"));
			} else {
				paintFill.setColor(Color.parseColor("#BC7C7C7C"));
				paintStroke.setColor(Color.parseColor("#007C7C7C"));
			}
		}
	}
	
	/*
	 void drawTrack(Canvas canvas, Track track, int zoomLevel, Point topLeftPoint) {
	 Rectangle canvasRectangle = new Rectangle(0, 0, canvas.getWidth(), canvas.getHeight());

	 int radiusInPixel = getRadiusInPixels(20, 0f, zoomLevel);

	 Paint paint = new Paint();
	 paint.setColor(track.color);
	 //		paint.setBitmapShaderShift(topLeftPoint);
	 paint.setStrokeWidth(radiusInPixel);

	 boolean isFirst = true;
	 int prevPixelX = -1;
	 int prevPixelY = -1;
	 for (Track.TrackItem t : track.items) {
	 double latitude = t.position.latitude;
	 double longitude = t.position.longitude;
	 long mapSize = MercatorProjection.getMapSize((byte)zoomLevel, tileSize);
	 int pixelX = (int) (MercatorProjection.longitudeToPixelX(longitude, mapSize) - topLeftPoint.x);
	 int pixelY = (int) (MercatorProjection.latitudeToPixelY(latitude, mapSize) - topLeftPoint.y);

	 // TODO: optimisation

	 if(isFirst) isFirst = false;
	 else canvas.drawLine(prevPixelX,prevPixelY,pixelX,pixelY,paint);

	 canvas.drawCircle(pixelX, pixelY, radiusInPixel, paint);

	 prevPixelX = pixelX;
	 prevPixelY = pixelY;
	 }
	 }	*/
}
