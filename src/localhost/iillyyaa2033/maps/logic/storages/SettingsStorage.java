package localhost.iillyyaa2033.maps.logic.storages;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.mapsforge.core.model.LatLong;

public class SettingsStorage{
	
	public static boolean ANDROID_MAP_SCALEBAR = false;
	public static boolean ANDROID_MAP_ZOOMCONTROLS = false;
	public static boolean ANDROID_MAP_LOCATION = false;
	public static boolean ANDROID_BEHAVIOR_SCROLL_TO_LOCATION = false;
	public static boolean ANDROID_BEHAVIOR_ZOOM_BY_VOLUME = false;
	public static boolean ANDROID_BEHAVIOR_TAP_FULLSCREEN = false;
	public static int ANDROID_MAP_TYPE = MapTypes.MAPNIK;
	
	public static boolean SW2_MAP_SCALEBAR = false;
	public static int SW2_MAP_TYPE = MapTypes.LOCAL;
	
	public static void updatePreferences(Context context){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		
		ANDROID_MAP_SCALEBAR = prefs.getBoolean("ANDROID_MAP_SCALEBAR", false);
		ANDROID_MAP_ZOOMCONTROLS = prefs.getBoolean("ANDROID_MAP_ZOOMCONTROLS", false);
		ANDROID_MAP_LOCATION = prefs.getBoolean("ANDROID_MAP_LOCATION", false);
		ANDROID_BEHAVIOR_SCROLL_TO_LOCATION = prefs.getBoolean("ANDROID_BEHAVIOR_SCROLL_TO_LOCATION", false);
		ANDROID_BEHAVIOR_ZOOM_BY_VOLUME = prefs.getBoolean("ANDROID_BEHAVIOR_ZOOM_BY_VOLUME", false);
		ANDROID_BEHAVIOR_TAP_FULLSCREEN = prefs.getBoolean("ANDROID_BEHAVIOR_TAP_FULLSCREEN", false);
	}
	
	public static File ANDROID_THEMEFILE = new File("");
	
	public static File SW2_MAPFILE = new File("");
	public static File SW2_THEMEFILE = new File("");
	
	public static void updateFiles(Context context){
		ANDROID_THEMEFILE = new File(context.getFilesDir(), "theme.xml");
		if(!ANDROID_THEMEFILE.exists()) copyFileFromApkToFilesDirInInternalStorage(context,"theme.xml");
		
		SW2_MAPFILE = new File(Environment.getExternalStorageDirectory(), Environment.DIRECTORY_DOWNLOADS+"/maps/map.map");
		SW2_THEMEFILE = new File(context.getFilesDir(),"theme.xml");
		if(!ANDROID_THEMEFILE.exists()) copyFileFromApkToFilesDirInInternalStorage(context,"theme.xml");
	}
	
	public static byte savedZoomLevel = 12;
	public static LatLong savedViewLatLong = new LatLong(59.938889, 30.315833);
	public static LatLong savedLocationLatLong = new LatLong(59.938889, 30.315833);
	
	public static void updateLocations(Context context){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		savedZoomLevel = (byte) prefs.getInt("ANDROID_SAVED_ZOOMLVL", 12);
		savedViewLatLong = new LatLong(prefs.getFloat("ANDROID_SAVED_LAT",59.938889f),prefs.getFloat("ANDROID_SAVED_LON",30.315833f));
		savedLocationLatLong = new LatLong(prefs.getFloat("ANDROID_SAVED_LOCATION_LAT",59.938889f),prefs.getFloat("ANDROID_SAVED_LOCATION_LON",30.315833f));
	}
	
	public static void saveLocations(Context context){
		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
		editor.putInt("ANDROID_SAVED_ZOOMLVL", savedZoomLevel);
		editor.putFloat("ANDROID_SAVED_LAT", (float) savedViewLatLong.latitude);
		editor.putFloat("ANDROID_SAVED_LON", (float) savedViewLatLong.longitude);
		editor.putFloat("ANDROID_SAVED_LOCATION_LAT", (float) savedLocationLatLong.latitude);
		editor.putFloat("ANDROID_SAVED_LOCATION_LON", (float) savedLocationLatLong.longitude);
		editor.apply();
		
	}
	
	public static void updateAll(Context context){
		updatePreferences(context);
		updateFiles(context);
		updateLocations(context);
	}
	
	public static void saveAll(Context context){
		saveLocations(context);
	}
	
	static File copyFileFromApkToFilesDirInInternalStorage(Context context,String filename){
		File resultFile = new File(context.getFilesDir(),filename);
		try {
			resultFile.createNewFile();
			InputStream from = context.getAssets().open(filename);
			OutputStream to = new FileOutputStream(resultFile);
			int buf = -1;
			while((buf = from.read()) != -1){
				to.write(buf);
			}
			to.flush();
			to.close();
			from.close();
		} catch (IOException e) {
			Toast.makeText(context,e.toString(),Toast.LENGTH_SHORT).show();
		}
		
		return resultFile;
	}
	
	//
	// one-time settings
	//
	public static boolean isTrackRecorderStarted = false;
	public static String providerForTracker = null;
	
	public class MapTypes{
		public static final int SAME_AS_DROID = -1;
		public static final int LOCAL = 0;
		public static final int MAPNIK = 1;
		public static final int USGS = 2;
	}
}
