package localhost.iillyyaa2033.maps.service;

import android.graphics.*;
import android.view.*;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.widget.Toast;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.sw2.MapExtension;
import org.osmdroid.views.MapView;


public class MapOverlayService extends Service {

	public static MapView map;
	public static volatile Bitmap bm;
	
	@Override
	public IBinder onBind(Intent p1) {
		// TODO: Implement this method
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		View root = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.service_overlay, null, false);
		map = (MapView) root.findViewById(R.id.service_overlay_MapView1);

		Toast.makeText(this, "Overlay service started", Toast.LENGTH_SHORT).show();

		WindowManager.LayoutParams params = 
			new WindowManager.LayoutParams(MapExtension.height, MapExtension.height, 
										   WindowManager.LayoutParams.TYPE_PHONE,
										   WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
										   WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
										   PixelFormat.TRANSLUCENT);
		params.gravity = Gravity.RIGHT | Gravity.CENTER;

		WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
		wm.addView(root, params);
		draw();
	}
	
	public static void draw(){
		if(bm==null){
			int width = 220, height = 176;
			bm = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
			//	bmp.compress(Bitmap.CompressFormat.PNG, 100, new ByteArrayOutputStream(256));
			bm.setDensity(DisplayMetrics.DENSITY_MEDIUM);
		}
		
		Canvas cnv = new Canvas(bm);
		
		map.measure(220,176);
		map.layout(0,0,220,176);
		map.invalidate();
		map.draw(cnv);
		
		
		Paint p = new Paint();
		p.setColor(Color.GREEN);
		cnv.drawText("MOS-txt"+System.currentTimeMillis(),10,10,p);
	}
}
