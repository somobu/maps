package localhost.iillyyaa2033.maps.interfaces;

import java.util.ArrayList;
import localhost.iillyyaa2033.maps.logic.items.SearchItem;

public interface OnSearchListener{
	
	public void search(String request, boolean inDatabase);
	public void setOnSearchListener(OnSearchResult listener);
	
	public static interface OnSearchResult{
        public void onResult(ArrayList<SearchItem> result);
    }
}
