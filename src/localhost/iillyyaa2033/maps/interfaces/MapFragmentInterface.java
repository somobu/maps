package localhost.iillyyaa2033.maps.interfaces;

import java.util.List;
import org.mapsforge.map.datastore.Way;
import org.mapsforge.core.model.LatLong;
import org.osmdroid.util.GeoPoint;

public interface MapFragmentInterface{
	
	public void onPause();
	public void resyncLayers();
	
	public void zoomIn();
	public void zoomOut();
	public void scrollTo(LatLong pos)
	
	public void setLocationEnabled(boolean isEnabled);
	public void setNewLocation(GeoPoint point, boolean isOnline);
	
	public List<Way> getVisibleWays();
}
