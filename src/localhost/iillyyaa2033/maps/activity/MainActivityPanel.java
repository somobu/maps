package localhost.iillyyaa2033.maps.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;
import java.util.List;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.fragment.MapsManagerFragment;
import localhost.iillyyaa2033.maps.fragment.LocationFragment;
import localhost.iillyyaa2033.maps.fragment.NotesListFragment;
import localhost.iillyyaa2033.maps.fragment.SearchFragment;
import localhost.iillyyaa2033.maps.fragment.TrackerFragment;
import localhost.iillyyaa2033.maps.interfaces.MapActivityInterface;
import localhost.iillyyaa2033.maps.interfaces.OnSearchListener;
import localhost.iillyyaa2033.maps.logic.items.SearchItem;
import localhost.iillyyaa2033.maps.logic.storages.LocalSearchedBuffer;
import localhost.iillyyaa2033.maps.logic.storages.SettingsStorage;
import localhost.iillyyaa2033.maps.service.TrackRecorderService;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.model.Tag;
import org.mapsforge.map.datastore.Way;
import localhost.iillyyaa2033.maps.fragment.LayersManagerFragment;

public class MainActivityPanel extends Activity implements MapActivityInterface {

	@Override
	public void startLayerman() {
		ActionBar ab = getActionBar();
		//	ab.setBackgroundDrawable(null);

		Fragment fragment = new LayersManagerFragment();
		ab.setTitle(R.string.android_fragment_location_provider_title);
		
		getFragmentManager()
			.beginTransaction()
			.add(R.id.activity_main_frame_secondary,fragment)
			.commit();
	}
	
	@Override
	public void toggleBars() {
		// TODO: Implement this method
	}

	@Override
	public void addNote(LatLong position) {
		// TODO: Implement this method
	}
	
	
	Intent result;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main_panel);
		
		ActionBar ab = getActionBar();
	//	ab.setBackgroundDrawable(null);
		
		Fragment fragment;
		switch(getIntent().getIntExtra("panelNum",0)){
			case 1:
				fragment = new LocationFragment();
				ab.setTitle(R.string.android_fragment_location_provider_title);
				break;
			case 2:
				fragment = new NotesListFragment();
				ab.setTitle(R.string.android_dialog_notes_list_title);
				break;
			case 3:
				fragment = new MapsManagerFragment();
				ab.setTitle(R.string.android_fragment_layersman_title);
				break;
			case 4:
				fragment = new SearchFragment();
				ab.setTitle(R.string.android_fragment_search_title);
				break;
			case 5:
				fragment = new TrackerFragment();
				ab.setTitle(R.string.android_fragment_tracker_title);
				break;
			default:
				fragment = new Fragment();
		}
		
		getFragmentManager()
			.beginTransaction()
			.add(R.id.activity_main_frame_secondary,fragment)
			.commit();
		
		result = new Intent();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0,0,0,R.string.android_activity_mainpanel_ab_close).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
			case 0:
				closePanel();
				break;
		}
		
		return true;
	}
	
	
	@Override
	public void redrawLayers() {
		result.putExtra("redrawLayers",true);
	}

	@Override
	public void resyncLayers() {
		result.putExtra("resyncLayers",true);
	}

	@Override
	public void closePanel() {
		setResult(Activity.RESULT_OK,result);
		finish();
	}

	@Override
	public void scroll(LatLong to) {
		scroll(to,true);
	}
		
	@Override
	public void scroll(LatLong to, boolean immediately) {
		result.putExtra("scroll",true);
		result.putExtra("scrollX",to.latitude);
		result.putExtra("scrollY",to.longitude);
		if(immediately) closePanel();
	}

	@Override
	public void listenFrom(String provider) {
		result.putExtra("listenFrom",true);
		result.putExtra("locationProvider",provider);
	}

	@Override
	public void startRecordTrack(String provider) {
		if(!SettingsStorage.isTrackRecorderStarted){
			SettingsStorage.providerForTracker = provider;
			startService(new Intent(this,TrackRecorderService.class));
			SettingsStorage.isTrackRecorderStarted = true;
		}
	}
	
	OnSearchListener.OnSearchResult searchListener;
	
	@Override
	public void search(String request, boolean inDatabase) {

		request = request.toLowerCase();

		ArrayList<SearchItem> list = new ArrayList<SearchItem>();

		List<Way> allNotes = LocalSearchedBuffer.items;
		String name = null;
		for (Way w : allNotes) {
			if ((name = haveTag(w, "name")) != null) {
				if(name.toLowerCase().contains(request)){
					SearchItem itm = new SearchItem(name,w.latLongs);
					list.add(itm);
				}
			}
		}
		searchListener.onResult(list);
	}

	String haveTag(Way w, String tag) {
		for (Tag t : w.tags) {
			if (t.key.equals(tag)) return t.value;
		}
		return null;
	}

	@Override
	public void setOnSearchListener(OnSearchListener.OnSearchResult listener) {
		searchListener = listener;
	}
}
