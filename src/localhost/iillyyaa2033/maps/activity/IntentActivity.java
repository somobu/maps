package localhost.iillyyaa2033.maps.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.Set;
import android.content.Intent;

public class IntentActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ScrollView root = new ScrollView(this);
		TextView text = new TextView(this);
		root.addView(text);
		setContentView(root);


		String two = "";
		
		Intent i = getIntent();
		two += "action: " + i.getAction()+"\n";
		two += "data: " +i.getDataString()+"\n";
		two += "scheme: "+i.getScheme()+"\n";
		
		Bundle bundle = i.getExtras();
		if (bundle != null) {
			Set<String> keys = bundle.keySet();
			
			for (String key : keys) {
				two += key + ": " + bundle.getString(key) + "\n";
			}
		} else {
			two += "no extras";
		}
		text.setText(two);
	}


}
