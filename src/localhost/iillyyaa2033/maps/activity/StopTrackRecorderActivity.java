package localhost.iillyyaa2033.maps.activity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import localhost.iillyyaa2033.maps.logic.storages.SettingsStorage;
import localhost.iillyyaa2033.maps.service.TrackRecorderService;

public class StopTrackRecorderActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		stopService(new Intent(this, TrackRecorderService.class));
		SettingsStorage.isTrackRecorderStarted = false;
		
		// TODO: show recorded track preferences, if needed
		
		finish();
	}
	
	
}
