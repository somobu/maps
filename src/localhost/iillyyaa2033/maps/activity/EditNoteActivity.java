package localhost.iillyyaa2033.maps.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.logic.items.Note;
import localhost.iillyyaa2033.maps.logic.storages.DatabaseStorage;
import localhost.iillyyaa2033.maps.logic.storages.SettingsStorage;
import org.mapsforge.core.model.LatLong;

public class EditNoteActivity extends Activity {

	View colorPeview;
	EditText colorName;
	int lastValidColor = 0xFFFFFF;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_editnote);
		
		Intent data = getIntent();
		final int noteid = data.getIntExtra("noteid",-1);
		
		final EditText name = (EditText) findViewById(R.id.activity_editnote_name);
		TextView coords = (TextView) findViewById(R.id.activity_editnote_coords);
		colorPeview = findViewById(R.id.activity_editnote_color_preview);
		colorName = (EditText) findViewById(R.id.activity_editnote_color_text);
		
		final double lat;
		final double lon;		
		if(noteid == -1){
			lat = data.getDoubleExtra("positionX",SettingsStorage.savedViewLatLong.latitude);
			lon = data.getDoubleExtra("positionY",SettingsStorage.savedViewLatLong.longitude);
		} else {
			Note n = DatabaseStorage.notes.get(noteid);
			name.setText(n.tag);
			lat = n.position.latitude;
			lon = n.position.longitude;
			lastValidColor = n.color;
			colorPeview.setBackgroundColor(lastValidColor);
		}
		
		coords.setText(lat + " : "+lon);
		
		Button colorPick = (Button) findViewById(R.id.activity_editnote_color_pick);
		colorPick.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					pickDialog();
				}
			});
			
		Button accept = (Button) findViewById(R.id.activity_editnote_accept);
		accept.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					if(noteid >= 0) DatabaseStorage.notes_remove(noteid);
					Note n = new Note(name.getText().toString(), new LatLong(lat,lon),lastValidColor);
					DatabaseStorage.notes_add(n);
					setResult(Activity.RESULT_OK);
					EditNoteActivity.this.finish();
				}
			});
	}
	
	void pickDialog(){
		ColorPickerDialogBuilder
			.with(this)
			.setTitle("Choose color")
			.initialColor(0xffffffff)
			.wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
			.density(12)
			.setOnColorSelectedListener(new OnColorSelectedListener() {
				@Override
				public void onColorSelected(int selectedColor) {
					lastValidColor = selectedColor;
				}
			})
			.setPositiveButton("Okey", new ColorPickerClickListener(){

				@Override
				public void onClick(DialogInterface d, int lastSelectedColor, Integer[] allColors) {
					lastValidColor = lastSelectedColor;
					colorPeview.setBackgroundColor(lastSelectedColor);
					colorName.setText("#"+Integer.toHexString(lastSelectedColor).toUpperCase());
				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){

				@Override
				public void onClick(DialogInterface p1, int p2) {
					
				}
			})
			.build()
			.show();
	}
	
	boolean validateAndUpdatePreview(String text){
		if(text.length() != 7) return false;
		if(!text.startsWith("#")) return false;
		
		int i = Integer.parseInt("0x"+text.substring(1));
		
		return true;
	}
}
