package localhost.iillyyaa2033.maps.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;
import localhost.iillyyaa2033.maps.fragment.OsmdroidMapFragment;
import localhost.iillyyaa2033.maps.interfaces.MapActivityInterface;
import localhost.iillyyaa2033.maps.logic.storages.SettingsStorage;
import localhost.iillyyaa2033.maps.service.TrackRecorderService;
import org.mapsforge.core.model.LatLong;
import localhost.iillyyaa2033.maps.fragment.LayersManagerFragment;

public class MainActivityDialog extends Activity implements MapActivityInterface{

	Intent result = new Intent();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		FrameLayout frame = new FrameLayout(this);
		frame.setId(15);
		setContentView(frame);
		getFragmentManager().beginTransaction().add(15,new LayersManagerFragment()).commit();
	}
	
	@Override
	public void redrawLayers() {
		result.putExtra("redrawLayers",true);
	}

	@Override
	public void resyncLayers() {
		result.putExtra("resyncLayers",true);
	}

	@Override
	public void closePanel() {
		setResult(Activity.RESULT_OK,result);
		finish();
	}

	@Override
	public void scroll(LatLong to) {
		scroll(to,true);
	}

	@Override
	public void scroll(LatLong to, boolean immediately) {
		result.putExtra("scroll",true);
		result.putExtra("scrollX",to.latitude);
		result.putExtra("scrollY",to.longitude);
		if(immediately) closePanel();
	}
	
	@Override
	public void listenFrom(String provider) {
		result.putExtra("listenFrom",true);
		result.putExtra("locationProvider",provider);
	}

	@Override
	public void startRecordTrack(String provider) {
		if(!SettingsStorage.isTrackRecorderStarted){
			SettingsStorage.providerForTracker = provider;
			startService(new Intent(this,TrackRecorderService.class));
			SettingsStorage.isTrackRecorderStarted = true;
		}
	}
	
	@Override
	public void toggleBars() {
		// TODO: Implement this method
	}

	@Override
	public void addNote(LatLong position) {
		// TODO: Implement this method
	}

	@Override
	public void startLayerman() {
		// TODO: Implement this method
	}

	@Override
	public void search(String request, boolean inDatabase) {
		// TODO: Implement this method
	}
}
