package localhost.iillyyaa2033.maps.activity;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.logic.storages.SettingsStorage;

public class PreferenceSW2Activity extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings_sw2);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		SettingsStorage.updatePreferences(this);
	}
}
