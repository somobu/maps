# maps
**Still under development!**

Android map based on [mapsforge](https://github.com/mapsforge/mapsforge).

Features:
- Maps:
  - Multiple maps loading support
  - Notes on map
- Location
  - Track recording
- Search on map (reverse geolocation)
- Sony Smartwatch 2 support

# Карты
**Проект все еще в разработке!**

Карты для Android, основанные на [Mapsforge](https://github.com/mapsforge/mapsforge).

Что есть:
- Собственно, карты:
  - Возможность загрузки нескольких карт
  - Пометки на карте
- Местоположение
  - Запись треков
- Поиск на карте (обратная геолокация)
- Поддержка Sony Smartwatch 2
